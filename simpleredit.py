#!/usr/bin/env python3
#Licensed under Sam Hocevar's WTFPL. You just do what the fuck you want to.
import argparse
import os
import shutil
import struct, array
import pickle
import pprint
import readline
import json
from clint.textui import colored
#TODO: Write doctests
# I should also adhere to some sort of consistency but whatever

#class Screen(object):
#    def __init__(self):
#        pass
spriteattr = ('x','y','seq','frame','type','size','active','rotation','special','brain','script','hit','die','talk','speed','basewalk','baseidle','baseattack','basehit','delay','depth','hardness','trimleft','trimtop','trimright','trimbottom','warpstatus','warpscreen','warpx','warpy','touchseq','basedeath','gold','hitpoints','strength','defense','experience','sound','vision','nohit','touchdamage','junk')

def newmap():
    '''Makes a new index and an empty map'''
    index = {}
    for i in ('screenno', 'midi', 'indoor'):
        index[i] = [0] * 768

    maps = []    
    return index, maps


def openmap(scrns):
    '''Opens a MAP.DAT file'''
    maps = []
    with open('map.dat', 'rb') as f:
        for i in range(scrns):
            screen = {}
            mapscreen = f.read(31280)
            for i in ('tileno','sprites','alt_hard'):
                screen[i] = []
            for j in range(96):
                screen['tileno'].append(struct.unpack('i',mapscreen[(80*j)+20: (80*j)+24])[0])
                screen['alt_hard'].append(struct.unpack('i',mapscreen[(80*j)+28: (80*j)+32])[0])

            for q in range(100):
                #WC's guide is a piece of shit. Dinkvar.c is a much better learning resource.
                #The script fields are indeed 13 bytes
                #Is the gap at the end 20 bytes?
                screen['sprites'].append(dict(zip(spriteattr, struct.unpack('10i13s13s13s13s29i',mapscreen[(220 * q) + 8240:(220 * q) + 8448]))))
            #Screen script field is 20 bytes long.
            screen['script'] = struct.unpack('20s', mapscreen[30240:30260])[0]
            #1020 bytes of data left.
            maps.append(screen)
    return maps


def openindex(ddat):
    '''Takes a Dink.dat file and loads its indexes and shit'''
    index = {}
    with open(ddat, 'rb') as f:
        f.read(24)
        #Each thing is four bytes
        index['screenno'] = list(struct.unpack('768i',f.read(4 * 768)))
        f.read(4)
        index['midi'] = list(struct.unpack('768i',f.read(4 * 768)))
        f.read(4)
        index['indoor'] = list(struct.unpack('768i', f.read(4 * 768)))
        #2244 bytes needed to pack it together to make it 11488
        return index

def getinput(curscrn,index,mapping):
    '''This parses the users commands and does stuff. Takes a screen number and map'''
    #I have no idea what I am doing here
    #Use this as an example of how not to do things
    indexscr = index['screenno'][curscrn -1]
    cmd = input(('%s, %s ->') % (curscrn, indexscr))
    if str(cmd) not in('exit','quit','q','gtfo'):
        
        if 'sprite' in cmd:
            if len(cmd.split()) > 1:
                try:
                    if (0 <= int(cmd.split()[1]) <100):
                        print(colored.yellow("Showing individual sprite information"))
                        spriteinfo(mapping[indexscr -1]['sprites'][int(cmd.split()[1])])
                except:
                    print(colored.red("Not a valid sprite number"))
            else:
                spriteinfo(mapping[indexscr -1])
        
        if cmd.startswith('screens'):
            #Show index
            if len(cmd.split()) > 1 and cmd.split()[1] == 'dinkedit':
                showscreens(index,dinkedit=True)
            else:
                showscreens(index)
                
        if 'tiles' in cmd.split():
        #Make the scrip run one time
            tileinfo(mapping[indexscr -1])

        if 'screen' in cmd.split():
            #Shows the current screen's properties or change screen
            if len(cmd.split()) > 1:
                try:
                    newscrn = int(cmd.split()[1])
                    if (0 < newscrn <= 768):
                        if index['screenno'][newscrn -1] == 0: 
                            print(colored.red('That screen does not exist'))                       
                        else:    
                            curscrn = int(cmd.split()[1])
                            print(colored.yellow("Changing the screen..."))
                except:
                    print(colored.red("Invalid screen number")) 
            # This appears to work for the time being
            mapscrinfo(mapping[index['screenno'][curscrn -1] -1])  
        
        if cmd.startswith('save'):
           #Save the map into a few different files including JSON, a pickled file and of course the dats
           print(colored.yellow("Saving the files..."))
           savefiles(index,mapping)
           
        if cmd.startswith('EDIT'):
            print(colored.yellow("Entering editing mode..."))
            index, mapping = editmode(index,mapping,curscrn)

        getinput(curscrn,index,mapping)
    else:
        print(colored.magenta("Exiting without saving!"))

def editmode(index,mapping,screen):
    '''Allows you to edit the properties of a screen'''
    #This is a jumbly bunch of shit innit.
    print(colored.magenta("You are now in edit mode. What would you like to do?"))
    print(colored.magenta("Avaliable choices are: newscreen, delscreen, sprite, screen, tile"))
    #Do stuff here
    scrn = input(colored.green("Enter the screen number you'd like to delete/edit/create ->"))
    if (0 < int(scrn) <= 768):
        print(colored.magenta("Choose a command"))
        command = input(colored.green("EDIT MODE->"))
        if command == 'newscreen':    
            index, mapping = newscreen(index,mapping,int(scrn))
        elif command == 'delscreen':
            index, mapping = delscreen(index,mapping,int(scrn))
        elif command == 'screen':
            print(colored.magenta("You may edit the screen's 'script', 'MIDI' integer, or 'indoor' status"))
            attr = input(colored.green("EDITING SCREEN %s->") % scrn)
            mapscr = index['screenno'][scrn -1]
            if attr.lower() == 'midi':
            midino = input(colored.green("Type in the MIDI integer you'd like to use: "))
            try:
                index['midi'][scrn-1] = int(midino)
            except:
                print(colored.red("That's not a valid MIDI integer"))
            elif attr == 'script':
                script = input(colored.green("Type in the screen's script (without extension) that you'd like to use: "))
                if mapscr != 0:
                    mapping[mapscr]['script'] = bytes.encode(script)
    else:
        print(colored.red("Invalid map number"))
            
    return index,mapping

def newscreen(index,mapping,scrn):
    '''Creates a new Dink map screen numbered scrn'''
    if index['screenno'][scrn-1] == 0:
        mapno = max(index['screenno'][0:scrn-1]) + 1
        index['screenno'][scrn-1] = mapno
        newscr = {'tileno': [0] * 100, 'alt_hard': [0] * 100, 'sprites': [0] * 100, 'script': b''}
        for i in range(100):
            newscr['sprites'][i] = {}
            for j in spriteattr:
                if j in ('script','hit','die','talk'):
                    newscr['sprites'][i][j] = b''
                else:
                    newscr['sprites'][i][j] = 0
        mapping.insert(mapno-1, newscr)
        for i in range(scrn, 768):
            if index['screenno'][i] > 0:
                index['screenno'][i] += 1
    else:
        print(colored.red("There's already a screen there"))
    
    return index,mapping

def delscreen(index,mapping,scrn):
    '''Deletes a screen numbered scrn'''
    #Refactor this into newscreen above
    screen = index['screenno'][scrn-1]
    print(colored.yellow("Deleting the screen..."))
    if screen != 0:
        mapping.pop(screen-1)
        index['screenno'][scrn-1] = 0
        for i in range(scrn,768):
            if index['screenno'][i] > 0:
                index['screenno'][i] -= 1
    else:
        print(colored.red("There's no screen at that location!"))
        
    return index,mapping    
            

def savefiles(index,mapping):
    '''Saves le files into DATs and JSON and Pickled files'''
    print(colored.yellow("Attempting to save Dink.dat, Map.dat, index.json and Map.dink..."))
    #TODO: Write sanity tests regarding values of stuff and clean this shit up
    #Save Dink.dat to disk
    name = struct.pack('24s',b'Smallwood')
    screenno = array.array('i')
    midi = array.array('i')
    indoor = array.array('i')
    index['screenno'].append(0)
    index['midi'].append(0)
    screenno.fromlist(index['screenno'])
    midi.fromlist(index['midi'])
    indoor.fromlist(index['indoor'])  
    padding = struct.pack('2240x')
    dinkdat = name + bytes(screenno + midi + indoor) + padding
    with open('dink.dat','wb') as f:
        f.write(dinkdat)  

    #shutil.copy('map.dat','map.dat.bak')
    #Write map.dat to disk
    with open('map.dat','wb') as f:
        for count,thing in enumerate(mapping):
            screen = struct.pack('20x')
            for i in range(8 * 12):
                screen += struct.pack('i', thing['tileno'][i])
                screen += struct.pack('4x')
                screen += struct.pack('i', thing['alt_hard'][i])
                screen += struct.pack('68x')    
            sprites = struct.pack('540x')
            for i in range(100):
                for j in (spriteattr):
                    if j in ('script','hit','die','talk'):
                        sprites += struct.pack('13s', thing['sprites'][i][j])
                    elif j == "junk":
                        sprites += struct.pack('20x')
                    else:    
                        sprites += struct.pack('i', thing['sprites'][i][j])
            sprites += struct.pack('20s', thing['script'])
            screen += sprites + struct.pack('1020x')
            f.write(screen)
    
    print(colored.yellow("Finished writing MAP.DAT"))
    mapping.append(index)
    with open('map.dink','wb') as f:    
        pickle.dump(mapping,f)
        
    with open('index.json', 'w') as f:
        json.dump(index,f,skipkeys=True, indent=4)

def showscreens(index,dinkedit=False):
    '''Prints representations of the map index properties'''
    for i in range(768):
        if dinkedit:    
            if (i % 32) == 0:
                print('\n')
            if index['screenno'][i] > 0:
                print(colored.red('%4s' % (i+1)), end='')
            else:
                print(colored.blue('%4s' % (i+1)), end='')
        else:
            print('Screen %3s, Numbered: %3d, Midi: %4d, Indoor status: %s' % (i+1, index['screenno'][i], index['midi'][i], bool(index['indoor'][i])))
    print('\n')

    
def mapscrinfo(mapscr):
    '''Prints the properties of a single map screen'''
    #The screen number in Dink.dat corresponds to the index in Map.dat
    print(colored.magenta("Current screen properties:"))
    scount = 0
    for i in mapscr['sprites']:
        if i['active'] == 1:
            scount += 1
    print("This screen has a script of %s and %s active sprites" % (bytes.decode(mapscr['script'], errors='replace'),scount))


def tileinfo(mapscr):
    '''Prints information about a screens tiles'''
    #Should I refactor this into mapscrinfo? Probably.
    # TODO tile decoding shit
    print(colored.magenta("Tiles values and hard.dat squares"))
    for i in ('tileno','alt_hard'):
        for count,thing in enumerate(mapscr[i]):
            if count % 12 == 0:
                print('\n')
            print(colored.cyan('%4s' % thing), end=' ')
        print('\n')
            
def spriteinfo(mapscr):
    '''Displays the sprite info for a given screen'''
    #Takes either a screen or an individual sprite
    # Should probably change the var name to something other than "mapscr"
    if 'x' in mapscr:
        #Display the info about a sprite
        printer = pprint.PrettyPrinter(indent=2)
        printer.pprint(mapscr)
    else:    
        #Show all info about sprites
        print(colored.magenta(' num | x  |  y  | seq  | frame  | brain | script'))
        for count,thing in enumerate(mapscr['sprites']):
            print(colored.cyan(count), end=' ')
            for i in ('x','y','seq','frame','brain'):
                print(repr(thing[i]).rjust(3), end='\t')
            print(bytes.decode(thing['script'],errors='replace'))

def main():
    parser = argparse.ArgumentParser(description="SimplerEdit - An editor and exporter for Dink Smallwood maps")
    parser.add_argument('-map', help='Specify a MAP.DAT file to open')
    parser.add_argument('-proj', help="Specifies a project file to open")
    parser.add_argument('-dinkdat', help='Specify a Dink.dat index file to open')
    args = parser.parse_args()
    if args.dinkdat:
        print(colored.yellow("Opening Dink.dat..."))
        index = openindex(args.dinkdat)
        scount = 0
        #Maybe scrap this and use the file iterator thingy instead
        for i,j in enumerate(index['screenno']):
            if j > 0:
               scount += 1
        print(colored.yellow("Opening Map.dat... Please wait."))
        if not os.path.exists(args.map):
            print(colored.red("Could not find MAP.DAT."))
            raise IOError
        else:
            mapping = openmap(scount)
    elif args.proj:
        print(colored.yellow("Loading project file..."))
        with open(args.proj, 'rb') as f:
            mapping = pickle.load(f)
            index = mapping[-1]
            del mapping[-1]
            
    else:
        print(colored.red("Please specify an option")) 
    getinput(1,index,mapping)

if __name__ == '__main__':
    main()
